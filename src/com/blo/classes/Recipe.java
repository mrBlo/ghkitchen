package com.blo.classes;

import java.io.Serializable;

public class Recipe implements Serializable{
	//Variables
private String username;
//private float cooking_time;
private String name,ingredients,procedures;
public Recipe() {
	super();
	// TODO Auto-generated constructor stub
}


public Recipe(String username, String name) {
	super();
	this.username = username;
	this.name = name;
}


public Recipe(String name,String username, String ingredients,String procedures) {
	super();
	this.username = username;
	this.name = name;
	this.ingredients = ingredients;
	this.procedures = procedures;
}


//public YourRecipe( String name,int consumers,float cooking_time,String ingredients,String procedures) {
//	super();
//	this.consumers = consumers;
//	this.name = name;
//	this.ingredients = ingredients;
//	this.procedures = procedures;
//	this.cooking_time=cooking_time;
//}






public String getUsername() {
	return username;
}


public void setUsername(String username) {
	this.username = username;
}


//public float getCooking_time() {
//	return cooking_time;
//}
//
//
//public void setCooking_time(float cooking_time) {
//	this.cooking_time = cooking_time;
//}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public String getIngredients() {
	return ingredients;
}


public void setIngredients(String ingredients) {
	this.ingredients = ingredients;
}


public String getProcedures() {
	return procedures;
}


public void setProcedures(String procedures) {
	this.procedures = procedures;
}


@Override
public String toString() {
	return "Recipe Name: " + name + "\nPosted by:  " + username 
//			"+,\nIngredients= " + ingredients +"\nProcedure= " + procedures
			;

			//+"\nCooking time= " + cooking_time
			
}


	
	
	
	
}
