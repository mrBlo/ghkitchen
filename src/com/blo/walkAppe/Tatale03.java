package com.blo.walkAppe;

//import info.androidhive.tabsswipe.R;
//import com.blo.kitchenGh.R;

import com.bpm.ghkitchen.R;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Tatale03 extends Fragment {
TextView tatale3countdown;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_tatale03, container, false);
		tatale3countdown=(TextView) rootView.findViewById(R.id.tatale3Countdown);
		new CountDownTimer(70000, 1000) {

		     public void onTick(long millisUntilFinished) {
		         tatale3countdown.setText("Seconds remaining: " + millisUntilFinished / 1000);
		     }

		     public void onFinish() {
		         tatale3countdown.setText("Done Cooking");
		     }
		  }.start();
		

		return rootView;
	}
	
}

