package com.blo.walkAppe;

//import info.androidhive.tabsswipe.adapter.TabsPagerAdapter;

import java.util.Locale;

import com.bpm.ghkitchen.R;
//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R.id;
//import com.blo.kitchenGh.R.layout;
//import com.blo.kitchenGh.R.menu;
//import com.blo.kitchenGh.R.string;
import com.me.serv.bgservice;
import com.will.appe.Tatale;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Tatale01 extends ActionBarActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	Intent intatale02;
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	private String[] tabs = { "Step 1", "Step 2", "Step 3" };
	
	Intent svc;

int numberOfPeople;
String numberOfPeopleString,dry,liquid;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tataleswipe);

		numberOfPeopleString = getIntent().getStringExtra("numberofpeople");
		dry=getIntent().getStringExtra("dry");
		liquid= getIntent().getStringExtra("liquid");
//Toast.makeText(getApplicationContext(), numberOfPeople+"", Toast.LENGTH_LONG).show();
		
//	intatale02= new Intent(getApplicationContext(), Tatale02.class);
//	intatale02.putExtra("numberofpeople", numberOfPeople);
	
		svc=new Intent(this, bgservice.class);
		startService(svc);
		
	
	
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		final ActionBar actionBar = getSupportActionBar();
		
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		mViewPager.setAdapter(mSectionsPagerAdapter);

		
		// Set up the action bar.
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// For each of the sections in the app, add a tab to the action bar.
				for (String tab_name : tabs) {
					actionBar.addTab(actionBar.newTab().setText(tab_name)
							.setTabListener(this));
				}

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
	
		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
////		getMenuInflater().inflate(R.menu.tatale01, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			//return PlaceholderFragment.newInstance(position + 1);
			
			switch (position) {
			case 0:
				// Top Rated fragment activity
				PlaceholderFragment ps=new PlaceholderFragment();
				return  ps;
			case 1:
				// Games fragment activity
				//startActivity(intatale02);
				Tatale02 tat2=new Tatale02();
				return tat2;
			case 2:
				// Games fragment activity
				Tatale03 tat3= new Tatale03();
				return tat3;
				
			}

			return null;
			
			
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		
		TextView tatale1countdown;
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_tatale01,
					container, false);
			tatale1countdown=(TextView) rootView.findViewById(R.id.tatale1Countdown);
			new CountDownTimer(60000, 1000) {

			     public void onTick(long millisUntilFinished) {
			         tatale1countdown.setText("Seconds remaining: " + millisUntilFinished / 1000);
			     }

			     public void onFinish() {
			         tatale1countdown.setText("Done!Swipe for next step");
			     }
			  }.start();
			

			
			return rootView;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
//	startActivity(new Intent(getApplicationContext(), Tatale.class));
//	finish();
	Intent in1=new Intent(Tatale01.this, Tatale.class);
	//in1.putExtra("numberofpeople", numberOfPeople);
	in1.putExtra("numberofpeople", numberOfPeopleString);
	in1.putExtra("dry", dry);
	in1.putExtra("liquid", liquid);
	
	stopService(svc);
	
	startActivity(in1); 
	Tatale01.this.finish();


}
	
}
