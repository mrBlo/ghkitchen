package com.will.appe;

//import com.blo.walkAppe.TataleWalk;
//import com.blo.kitchenGh.R;
//import com.blo.walkAppe.Tatale01;
////import com.blo.walkAppe.Tatale1;
////import com.example.cookapp1.R;
import com.blo.walkAppe.Tatale01;
import com.bpm.ghkitchen.Appetizer;
import com.bpm.ghkitchen.R;
import com.me.serv.bgservice;
//import com.will.cook.Appetizer;
//import com.will.cook.AfricanHomePage;
//import com.will.cook.MyRecipesPage;
//import com.will.cook.NumberOfPeoplePage;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Tatale extends Activity{
	AlertDialog.Builder builder;

TextView tv1,tvingre1,tvingre2,tvingre3,tvingre4,tvingre5,tvingre6,tv3,tv4,tv5;
Button LetsCook;
ImageView iv;
//ImageButton fav;
Intent svc;
String numberOfPeopleString,dry,liquid;


@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tatale);
		//Getting Bundle from Previous Page
		numberOfPeopleString = getIntent().getStringExtra("numberofpeople");
		dry = getIntent().getStringExtra("dry");
		liquid = getIntent().getStringExtra("liquid");
		
		
	//	Toast.makeText(getApplicationContext(), dry+"\n"+liquid, Toast.LENGTH_LONG).show();
		
		
		tv1=(TextView) findViewById(R.id.tataleIngredients);
		tvingre1=(TextView) findViewById(R.id.tataleIngredients1);
		tvingre2=(TextView) findViewById(R.id.tataleIngredients2);
		tvingre3=(TextView) findViewById(R.id.tataleIngredients3);
		tvingre4=(TextView) findViewById(R.id.tataleIngredients4);
		tvingre5=(TextView) findViewById(R.id.tataleIngredients5);
		tvingre6=(TextView) findViewById(R.id.tataleIngredients6);
		
		tv3=(TextView) findViewById(R.id.tataleProcedure);
		tv4=(TextView) findViewById(R.id.tataleProcedure1);
		LetsCook= (Button) findViewById(R.id.tatalewalkMeThrough);
		iv=(ImageView) findViewById(R.id.tataleimg);
	//	fav=(ImageButton) findViewById(R.id.TatalefavoriteButton);
				
		
		//Algorithm comprising how to multipy quantities by people
		//Ingredients
		
		int numberOfPeople= Integer.parseInt(numberOfPeopleString);
		
		//Checking for Measurements
		if (dry.matches("Cups")) {
			String ingre1=2*numberOfPeople+" over-ripe medium plantains(soft)";
			tvingre1.setText(ingre1);
			String ingre3="self-raising flour "+1*numberOfPeople*0.06+" cup(s)";
			//0.0625		1/16
			tvingre3.setText(ingre3);

			String ingre2=1*numberOfPeople+" small onion,finely chopped";
			tvingre2.setText(ingre2);
			
			//Normal setting of text,no algorithms
			tvingre4.setText("palm oil (optional) ");
	        tvingre5.setText("salt and hot pepper to taste ");
	        tvingre6.setText("vegetable oil");
		} 
		else if (dry.matches("Grams")) {
			String ingre1=2*numberOfPeople+" over-ripe medium plantains(soft)";
			tvingre1.setText(ingre1);
			String ingre3="self-raising flour "+1*numberOfPeople*14+" grams";
			tvingre3.setText(ingre3);

			String ingre2=1*numberOfPeople+" small onion,finely chopped";
			tvingre2.setText(ingre2);
			
			//Normal setting of text,no algorithms
			tvingre4.setText("palm oil (optional) ");
	        tvingre5.setText("salt and hot pepper to taste ");
	        tvingre6.setText("vegetable oil");
		}
		else if (dry.matches("Pounds")) {
			String ingre1=2*numberOfPeople+" over-ripe medium plantains(soft)";
			tvingre1.setText(ingre1);
			String ingre3="self-raising flour "+1*numberOfPeople*(0.03)+" pounds";
			//0.03125 	1/32
			tvingre3.setText(ingre3);

			String ingre2=1*numberOfPeople+" small onion,finely chopped";
			tvingre2.setText(ingre2);
			
			//Normal setting of text,no algorithms
			tvingre4.setText("palm oil (optional) ");
	        tvingre5.setText("salt and hot pepper to taste ");
	        tvingre6.setText("vegetable oil");
		}
		else if (dry.matches("Teaspoon")) {
			String ingre1=2*numberOfPeople+" over-ripe medium plantains(soft)";
			tvingre1.setText(ingre1);
			String ingre3="self-raising flour "+1*numberOfPeople*3+" teaspoons";
			tvingre3.setText(ingre3);
			String ingre2=1*numberOfPeople+" small onion,finely chopped";
			tvingre2.setText(ingre2);
			
			//Normal setting of text,no algorithms
			tvingre4.setText("palm oil (optional) ");
	        tvingre5.setText("salt and hot pepper to taste ");
	        tvingre6.setText("vegetable oil");
		}
		else if(dry.matches("Tablespoon")){
			String ingre1=2*numberOfPeople+" over-ripe medium plantains(soft)";
			tvingre1.setText(ingre1);
			String ingre3="self-raising flour "+1*numberOfPeople+" tablespoon(s)";
			tvingre3.setText(ingre3);
			String ingre2=1*numberOfPeople+" small onion,finely chopped";
			tvingre2.setText(ingre2);
			
			//Normal setting of text,no algorithms
			tvingre4.setText("palm oil (optional) ");
	        tvingre5.setText("salt and hot pepper to taste ");
	        tvingre6.setText("vegetable oil");
		}
		else {
			
		}
		
		
		
		//Toast.makeText(getApplicationContext(), ingre1, Toast.LENGTH_LONG).show();
		
		
		svc=new Intent(this, bgservice.class);
		stopService(svc);
		
		
//		fav.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Toast.makeText(getApplicationContext(), "Added to Favorites", Toast.LENGTH_LONG).show();
//			}
//		});
		
		LetsCook.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Let's Do Some Cooking", Toast.LENGTH_LONG).show();

				Intent in1=new Intent(Tatale.this, Tatale01.class);
				in1.putExtra("numberofpeople", numberOfPeopleString);
				in1.putExtra("dry", dry);
				in1.putExtra("liquid", liquid);
				startActivity(in1);
				//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
Tatale.this.finish();
			}
		});
			
		
		
		
	}


//OPTIONS MENU
//@Override
//public boolean onCreateOptionsMenu(Menu menu) {
//	// TODO Auto-generated method stub
//	MenuInflater inflater=getMenuInflater();
//	inflater.inflate(R.menu.myoptionsmenu, menu);
//	return true;
//}

//@Override
//public boolean onOptionsItemSelected(MenuItem item) {
//	// TODO Auto-generated method stub
//	if (item.getItemId()==R.id.HomePageMenuItem) {
//		//Toast.makeText(getApplicationContext(), "You are on the homepage", Toast.LENGTH_LONG).show();
//		startActivity(new Intent(getApplicationContext(),AfricanHomePage.class));
//		overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//		Tatale.this.finish();
//	}
//	else if (item.getItemId()==R.id.settingsMenuItem){
//		
//	}
//	
//	else if (item.getItemId()==R.id.exitMenuItem) {
//		builder=new AlertDialog.Builder(Tatale.this);
//		builder.setMessage("Are you sure you want to exit?");
//		builder.setTitle("ALERT");
//		builder.setCancelable(false);
//		builder.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
//		builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
//			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//			//	stopService(svc);
//				Tatale.this.finish();
//				System.exit(0);
//			}
//		});
//		
//		builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				dialog.cancel();
//			}
//		});
//		AlertDialog alert=builder.create();
//		alert.show();
//		
//	}
//	
//else if (item.getItemId()==R.id.myRecipesMenuItem) {
//	startActivity(new Intent(getApplicationContext(),MyRecipesPage.class));
//	overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//	Tatale.this.finish();
//
//	}
//else if (item.getItemId()==R.id.searchRecipeMenuItem) {
//
//
////
////
////
////
////
//
//
//
//
//}
//	
//	else {
//Toast.makeText(getApplicationContext(), "make right choice",Toast.LENGTH_LONG).show();
//	}
//	return super.onOptionsItemSelected(item);
//}



	
	
	@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			//super.onBackPressed();
		startActivity(new Intent(getApplicationContext(),Appetizer.class));
		//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
		Tatale.this.finish();
		}
	
}
