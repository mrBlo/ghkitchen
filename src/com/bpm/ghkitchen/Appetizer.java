package com.bpm.ghkitchen;

import java.util.ArrayList;

import com.blo.classes.MainListView;
//import com.blo.kitchenGh.R;
import com.bpm.ghkitchen.AfricanPage.MyGridWeatherAdaptor;
//import com.will.cook.Appetizer;
//import com.will.cook.NumberOfPeoplePage;
//import com.blo.kitchenGh.R;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Appetizer extends ActionBarActivity {
	GridView gv;
	AlertDialog.Builder builder;
	ArrayList<MainListView> photos = new ArrayList<MainListView>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appetizer);
		
		gv = (GridView) findViewById(R.id.weathergrid);
		ArrayList<MainListView> photos = new ArrayList<MainListView>();
		photos.add(new MainListView("Tatale", R.drawable.tatale));
		photos.add(new MainListView("Kelewele", R.drawable.kelewele));
		photos.add(new MainListView("Chicken Soup", R.drawable.chickensoup));
		
		MyGridWeatherAdaptor gad = new MyGridWeatherAdaptor(this, photos);
		gv.setAdapter(gad);	
gv.setOnItemClickListener(new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		MainListView mem=(MainListView)parent.getItemAtPosition(position);
		final String selectName=mem.getName();
		Log.d("WATCH THIS", selectName);
		
		if(selectName.equals("Tatale")){
			startActivity(new Intent(getApplicationContext(), NumberOfPeoplePage.class));
			//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			Appetizer.this.finish();
		}
		else if(selectName.equals("Kelewele")){
			
		}
else if(selectName.equals("Chicken Soup")){
			
		}
else{
Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
}
		
		
	}
} );

		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.appetizer, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

class MyGridWeatherAdaptor extends BaseAdapter
{
	Context ctx;
	ArrayList<MainListView> ctrys;
	
	public MyGridWeatherAdaptor(Context ctx, ArrayList<MainListView> ctrys) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.ctrys = ctrys;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ctrys.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ctrys.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MainListView ctry = ctrys.get(position);
		LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
	//	View v = linf.inflate(R.layout.gridview, null);
		convertView = linf.inflate(R.layout.gridview, null);
		
		ImageView photo = (ImageView) convertView.findViewById(R.id.pp);
	//	TextView details = (TextView) v.findViewById(R.id.weatherinfo);
		TextView name = (TextView) convertView.findViewById(R.id.stdinfo);
		photo.setImageResource(ctry.getImage());
		//details.setText(ctry.getTemp()+"*C");
		name.setText(ctry.getName());
		
		return convertView;

	}	
}


@Override
public void onBackPressed() {
	startActivity(new Intent(Appetizer.this, AfricanPage.class));
	//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
	Appetizer.this.finish();
	
}

	
	
}
