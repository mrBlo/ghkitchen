package com.bpm.ghkitchen;

import java.util.ArrayList;

import com.blo.classes.MainListView;
//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R;
import com.bpm.ghkitchen.HomePage.MyGridWeatherAdaptor;
//import com.will.cook.AfricanHomePage;
//import com.will.cook.Appetizer;
//import com.will.cook.AfricanHomePage;
//import com.will.cook.HomePage;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AfricanPage extends ActionBarActivity {
	GridView gv;
	AlertDialog.Builder builder;
	ArrayList<MainListView> photos = new ArrayList<MainListView>();
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.african_page);
		gv = (GridView) findViewById(R.id.weathergrid);
//		ArrayList<MainListView> photos = new ArrayList<MainListView>();
		photos.add(new MainListView("Breakfast", R.drawable.breakfasttt));
		photos.add(new MainListView("Lunch", R.drawable.lunch));
		photos.add(new MainListView("Supper", R.drawable.supper));
		photos.add(new MainListView("Appetizer", R.drawable.appetizer));
		photos.add(new MainListView("Dessert", R.drawable.dessert));
		photos.add(new MainListView("Snacks", R.drawable.snacks));
		photos.add(new MainListView("Drinks", R.drawable.smoothies));
		



//
		MyGridWeatherAdaptor gad = new MyGridWeatherAdaptor(this, photos);
		gv.setAdapter(gad);	
gv.setOnItemClickListener(new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		MainListView mem=(MainListView)parent.getItemAtPosition(position);
		final String selectName=mem.getName();
		Log.d("WATCH THIS", selectName);
		
		if(selectName.equals("Breakfast")){
			//Toast.makeText(getApplicationContext(), "Selected Input", Toast.LENGTH_LONG).show();
			
		}
		else if(selectName.equals("Lunch")){
			
		}
else if(selectName.equals("Supper")){
			
		}
else if(selectName.equals("Appetizer")){
startActivity(new Intent(AfricanPage.this, Appetizer.class));
////overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
AfricanPage.this.finish();
}

else if(selectName.equals("Dessert")){
//startActivity(new Intent(getApplicationContext(), Appetizer.class));
//AfricanHomePage.this.finish();
}

else if(selectName.equals("Snacks")){
//startActivity(new Intent(getApplicationContext(), Appetizer.class));
//AfricanHomePage.this.finish();
}

else if(selectName.equals("Drinks")){
//startActivity(new Intent(getApplicationContext(), Appetizer.class));
//AfricanHomePage.this.finish();
}


else{
Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
}
		
		
	}
} );

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.african_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

class MyGridWeatherAdaptor extends BaseAdapter
{
	Context ctx;
	ArrayList<MainListView> ctrys;
	
	public MyGridWeatherAdaptor(Context ctx, ArrayList<MainListView> ctrys) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.ctrys = ctrys;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ctrys.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ctrys.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MainListView ctry = ctrys.get(position);
		LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
	//	View v = linf.inflate(R.layout.gridview, null);
		convertView = linf.inflate(R.layout.gridview, null);
		
		ImageView photo = (ImageView) convertView.findViewById(R.id.pp);
	//	TextView details = (TextView) v.findViewById(R.id.weatherinfo);
		TextView name = (TextView) convertView.findViewById(R.id.stdinfo);
		photo.setImageResource(ctry.getImage());
		//details.setText(ctry.getTemp()+"*C");
		name.setText(ctry.getName());
		
		return convertView;

	}	
	
}


@Override
public void onBackPressed() {
	startActivity(new Intent(AfricanPage.this, HomePage.class));
	//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
	AfricanPage.this.finish();
	
}

}
