package com.bpm.ghkitchen;

//import com.will.cook.HomePage;
//import com.will.cook.SplashActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		Thread background = new Thread() {
			public void run() {
				
				try {
					// Thread will sleep for 5 seconds
					sleep(1*1000);
					
					// After 5 seconds redirect to another intent
				    Intent i=new Intent(getBaseContext(),HomePage.class);
					startActivity(i);
					
					//Remove activity
					SplashActivity.this.finish();
					
				} catch (Exception e) {
				
				}
			}
		};
		
		// start thread
		background.start();


	}
}
