package com.bpm.ghkitchen;

import java.util.ArrayList;
import java.util.Locale;

import com.blo.classes.MainListView;
import com.bpm.features.HealthPage;
import com.bpm.features.Settings;
import com.bpm.user.UserPage1;
//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R;
//import com.will.cook.AfricanHomePage;
//import com.will.cook.HomePage;
//import com.will.classes.Country;
//import com.will.listview.MyGridWeatherAdaptor;
//import com.will.lview.R;
//import com.will.cook.HomePage.MyStdAdapter;
//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R;
//import com.will.cook.Appetizer;
//import com.will.cook.NumberOfPeoplePage;
//import com.will.cook.Appetizer.OurAdapter;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class HomePage extends ActionBarActivity {
	GridView gv;
	AlertDialog.Builder builder;
	ArrayList<MainListView> photos = new ArrayList<MainListView>();
	
	public static String texts= "Welcome to GH Kitchen";

	TextToSpeech tts;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homepage);
		tts= new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
			
			@Override
			public void onInit(int status) {
				// TODO Auto-generated method stub
				if(status!=TextToSpeech.ERROR)
				{
					tts.setLanguage(Locale.US);
				}		
				
			}
		});
		
		tts.speak(texts, TextToSpeech.QUEUE_FLUSH, null);
		
		
		
		
		gv = (GridView) findViewById(R.id.weathergrid);
				ArrayList<MainListView> photos = new ArrayList<MainListView>();
				photos.add(new MainListView("African Dishes", R.drawable.indexop));
				photos.add(new MainListView("Continental Dishes", R.drawable.burger));
				photos.add(new MainListView("Rechauffe Dishes", R.drawable.lunch));
				photos.add(new MainListView("Kids Lunch Box", R.drawable.kids));
				
				photos.add(new MainListView("Health Tips", R.drawable.healthokay));
				photos.add(new MainListView("My Profile", R.drawable.blanprofile));
				photos.add(new MainListView("Settings", R.drawable.settings));
				photos.add(new MainListView("About Us", R.drawable.aboutfresh));

		
		
		
//		
				MyGridWeatherAdaptor gad = new MyGridWeatherAdaptor(this, photos);
				gv.setAdapter(gad);	
	    gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				MainListView mem=(MainListView)parent.getItemAtPosition(position);
				final String selectName=mem.getName();
				Log.d("WATCH THIS", selectName);
				if(selectName.equals("African Dishes")){
					//Toast.makeText(getApplicationContext(), "Selected Input", Toast.LENGTH_LONG).show();
//				startActivity(new Intent(HomePage.this, AfricanPage.class));
////				//	overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//				HomePage.this.finish();	
					startActivity(new Intent(getApplicationContext(), AfricanPage.class));
					HomePage.this.finish();
				}
			
				else if(selectName.equals("Continental Dishes")){
					
				}
				
else if(selectName.equals("Rechauffe Dishes")){
					
				}
else if(selectName.equals("Health Tips")){
	startActivity(new Intent(getApplicationContext(), HealthPage.class));
	HomePage.this.finish();

}
else if(selectName.equals("Kids Lunch Box")){
	
}
else if(selectName.equals("My Profile")){
	startActivity(new Intent(getApplicationContext(), UserPage1.class));
	HomePage.this.finish();

}
else if(selectName.equals("Settings")){
	startActivity(new Intent(getApplicationContext(), Settings.class));
	HomePage.this.finish();

}
else if(selectName.equals("About Us")){
	
}

				
				
				
				else{
					Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
				}
				
				
			}
		} );
		}
//
//		
@Override
protected void onPause() {
	// TODO Auto-generated method stub
	if (tts!=null) {
		tts.stop();
		tts.shutdown();
	}
	super.onPause();
}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

class MyGridWeatherAdaptor extends BaseAdapter
{
	Context ctx;
	ArrayList<MainListView> ctrys;
	
	public MyGridWeatherAdaptor(Context ctx, ArrayList<MainListView> ctrys) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.ctrys = ctrys;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ctrys.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ctrys.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MainListView ctry = ctrys.get(position);
		LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
	//	View v = linf.inflate(R.layout.gridview, null);
		convertView = linf.inflate(R.layout.gridview, null);
		
		ImageView photo = (ImageView) convertView.findViewById(R.id.pp);
	//	TextView details = (TextView) v.findViewById(R.id.weatherinfo);
		TextView name = (TextView) convertView.findViewById(R.id.stdinfo);
		photo.setImageResource(ctry.getImage());
		//details.setText(ctry.getTemp()+"*C");
		name.setText(ctry.getName());
		
		return convertView;
	}

	
	}	
		
	
	
	

@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	//super.onBackPressed();
	builder=new AlertDialog.Builder(HomePage.this);
	builder.setMessage("Are you sure you want to exit?");
	builder.setTitle("ALERT");
	builder.setCancelable(false);
	builder.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
	builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
		//	stopService(svc);
			HomePage.this.finish();
			System.exit(0);
		}
	});
	
	builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			dialog.cancel();
		}
	});
	AlertDialog alert=builder.create();
	alert.show();
	
}


}
