package com.bpm.ghkitchen;

import java.util.ArrayList;
import java.util.HashMap;

import com.bpm.features.Settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class IngredientSubstitute extends Activity {
	private ListView lv;
	ArrayAdapter<String> adapter;
	AutoCompleteTextView inputSearch;
	//ArrayList< HashMap<String,String>> productList;
	//String products[];
	Button search,substitute;
	EditText subs;
	String ingre,ingrenew;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ingredient_substitute);

		
		 inputSearch = (AutoCompleteTextView) findViewById(R.id.ingredientsub); 
search= (Button) findViewById(R.id.ingredientSearchbutton);
subs=(EditText) findViewById(R.id.substitueeditText);
	substitute=(Button) findViewById(R.id.substitutebutton);
	
	subs.setVisibility(View.GONE);
	substitute.setVisibility(View.GONE);
	subs.setEnabled(false);
	
	
	//FIRST TIME RUN
	
		  boolean firstrun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("firstrun", true);
		    if (firstrun){
		 //  [[ ...Display the dialog ]]
				   new AlertDialog.Builder(this).setTitle("Notice").setMessage("This secton is to help with users who are\n" +
				   		"allergic to particular ingredients and would like\nto change with a preferred ingredient\n" +
				   		"Other ingredient substitutions can be made according to \nthe users preference")
				   .setNeutralButton("OK", null)
				   .setIcon(R.drawable.iconfresh).show();

		    // Save the state
		    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
		        .edit()
		        .putBoolean("firstrun", false)
		        .commit();
		    }
		    

			String products[]={"Honey", "Tomatoes", "Lemon juice", 
								"Butter", "Yoghurt",
								"Brown Rice","Diary Milk","Skimmed Milk","Evaporated Milk",
								"Vinegar", "Sugar",
								"Ketchup", "White Rice", "Orange juice", "Onion","Shallot","Buttermilk"};
			
		    
		    		    adapter = new ArrayAdapter
		    		   (this,android.R.layout.simple_list_item_1,products);
		    		   inputSearch.setAdapter(adapter);
		   
search.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		ingre=inputSearch.getText().toString();
		ingre.replace(" ", "");
		
		if(inputSearch.getText().toString().isEmpty()){
		Toast.makeText(getApplicationContext(), "Please fill blank box", Toast.LENGTH_LONG).show();
		}
		else{
		
		
		Toast.makeText(getApplicationContext(), ingre, Toast.LENGTH_LONG).show();
		
		subs.setVisibility(View.VISIBLE);
		
		if(ingre.equalsIgnoreCase("WhiteRice")){
			
			subs.setText("Brown Rice");
			substitute.setVisibility(View.VISIBLE);
		}
		else if(ingre.equalsIgnoreCase("Sugar")){
			subs.setText("Honey");
			substitute.setVisibility(View.VISIBLE);
		}
		
		else{
			Toast.makeText(getApplicationContext(), "Sorry we don't have your ingredient " +
					"in the substitute list", 8000).show();
		}
		
		
	
		}
	}
});

		
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		startActivity(new Intent(getApplicationContext(), Settings.class));
		//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
		IngredientSubstitute.this.finish();
	}

	
	
}
