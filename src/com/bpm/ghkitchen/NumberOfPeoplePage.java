package com.bpm.ghkitchen;

//import com.example.mydialog.R;
//import com.project.countdown.CountdownActivity;
//import com.will.cook.HomePage;

import java.util.ArrayList;

import com.will.appe.Tatale;
//import com.blo.kitchenGh.R;
////ort com.proj.trans.AdminPage;
////import com.proj.trans.AdminPage;
//import com.will.appe.Tatale;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class NumberOfPeoplePage extends Activity{

	TextView tv,drytv,liquidtv;
	Spinner drySpinner,liquidSpinner;
	Button b1,b2,b,procede;
	NumberPicker np;
	EditText numberEditText;
RadioGroup dryRadioGroup,liquidRadioGroup;
	String numberOfPeople,dry,liquid; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.numberofpeople);
		
		
		
		
		
		tv = (TextView) findViewById(R.id.textview1);
		drytv = (TextView) findViewById(R.id.drytextview);
		liquidtv = (TextView) findViewById(R.id.liquidtextview);
		
		 b= (Button) findViewById(R.id.numberOfPeopleOpenbutton);
		 procede= (Button) findViewById(R.id.procedebutton);
		 numberEditText= (EditText) findViewById(R.id.numberedittext);
		 numberEditText.setEnabled(false);
		 
		//HIDING KEYBOARD
			InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
imm.hideSoftInputFromWindow(b.getWindowToken(), 0);

		 
		 
		 
		dryRadioGroup=(RadioGroup) findViewById(R.id.dryradiogroup) ;
		liquidRadioGroup=(RadioGroup) findViewById(R.id.liquidradiogroup) ;
		
		 
		 dryRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup arg0, int arg1) {
					// TODO Auto-generated method stub
		
					switch (arg1) {
					case R.id.dryCup:
						dry="Cups";
						break;

					case R.id.dryGrams:
						dry="Grams";
						break;
						
					case R.id.dryPounds:
						dry="Pounds";
						break;

					case R.id.dryTablespoon:
						dry="Tablespoon";
						break;
					case R.id.dryTeaspoon:
						dry="Teaspoon";
						break;
					}
					Log.d("   mama we madde it     ", dry);
				}
			});
		
		 liquidRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(RadioGroup arg0, int arg1) {
					// TODO Auto-generated method stub
		
					switch (arg1) {
					case R.id.liquidCup:
						liquid="Cups";
						break;

					case R.id.liquidMilliLitres:
						liquid="Milli Litres";
						break;
						
					case R.id.liquidOunces:
						liquid="Ounces";
						break;

					case R.id.liquidPints:
						liquid="Pints";
						break;
					case R.id.liquidTablespoon:
						liquid="Tablespoon";
						break;
					case R.id.liquidTeaspoon:
						liquid="Teaspoon";
						break;
				
					}
				
				}
			});

		
		//On click of button display dialog
		 //My Spinners
//		 drySpinner=(Spinner) findViewById(R.id.dryspinner);
//		 liquidSpinner=(Spinner) findViewById(R.id.liquidspinner);
//		
		 //ArrayList for Dry Spinner 
//		 final ArrayList<String> dryArrayList = new ArrayList<String>();
//			//dryArrayList.add("");
//			 dryArrayList.add("Cups");
//			 dryArrayList.add("Table Spoons");
//			 dryArrayList.add("Grams");
//			 dryArrayList.add("Pounds");
//			 dryArrayList.add("Tea Spoons");
//			
//			 //ArrayAdapter for Dry Spinner
//				ArrayAdapter<String>ad= new ArrayAdapter<String>(NumberOfPeoplePage.this, R.layout.measurementspinnertextview, dryArrayList);
//				drySpinner.setAdapter(ad);
//				
//				
//				
//				
//				
//				//Liquid Measurements
//					 final ArrayList<String> LiquidArrayList = new ArrayList<String>();
//						//LiquidArrayList.add("");
//						LiquidArrayList.add("Cups");
//						LiquidArrayList.add("Table Spoons");
//						LiquidArrayList.add("Tea Spoons");
//						LiquidArrayList.add("Pints");
//						LiquidArrayList.add("Ounces");
//						LiquidArrayList.add("Milli Litres");
//						
//						
//					
//						
//						//Array Adapter for Liquid Spinner
//						ArrayAdapter<String>ad1= new ArrayAdapter<String>(NumberOfPeoplePage.this, R.layout.measurementspinnertextview, LiquidArrayList);
//					liquidSpinner.setAdapter(ad1);
//				
		 
		
		b.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//show();
				final Dialog dil = new Dialog(NumberOfPeoplePage.this);
				dil.setContentView(R.layout.numberofpeopledialog);
				dil.setTitle("Pick Number");
				dil.setCancelable(false);
				np = (NumberPicker) dil.findViewById(R.id.numberpicker1);
			b1 = (Button) dil.findViewById(R.id.numberOfPeopleOkayButton);
b2=(Button) dil.findViewById(R.id.numberOfPeopleCancelButton);


np.setMaxValue(20);
np.setMinValue(1);


b1.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	
		numberEditText.setText(String.valueOf(np.getValue()));

		//////
//		String fresh=String.valueOf(np.getValue());
//		Intent in1=new Intent(NumberOfPeoplePage.this, Tatale.class);
//		in1.putExtra("numberofpeople", fresh);
//		startActivity(in1);
//	
		////
		dil.cancel();
	//	NumberOfPeoplePage.this.finish();
	}
});

b2.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	//	Toast.makeText(getApplicationContext(), "You should try the recipe", Toast.LENGTH_LONG).show();
		dil.cancel();
	}
});

dil.show();
			}
			
			
			
		});
		

//				
		
				//Procede button Onclick
				procede.setOnClickListener(new OnClickListener() {
					
					
					@Override
					public void onClick(View arg0) {
					numberOfPeople=numberEditText.getText().toString();
					
					
					
						
					
					
						
//					liquidSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//						//
//													@Override
//													public void onItemSelected(AdapterView<?> arg0,
//															View arg1, int index, long arg3) {
//														// TODO Auto-generated method stub
//														liquid=LiquidArrayList.get(index).toString();
////														
//													}
//						
//													@Override
//													public void onNothingSelected(
//															AdapterView<?> arg0) {
//													}
//												});
//
					
						if (numberOfPeople.isEmpty()) {
							Toast.makeText(getApplicationContext(), "Pick A Number", Toast.LENGTH_LONG).show();
						} 
						else if (dry==null || dry.matches("")||dry.isEmpty()) {
								Toast.makeText(getApplicationContext(), "Select Dry Measurement", Toast.LENGTH_LONG).show();
								
							} 
							else if (liquid==null || liquid.matches("") || liquid.isEmpty()) {
								Toast.makeText(getApplicationContext(), "Select Liquid Measurement", Toast.LENGTH_LONG).show();
							}

						
						
							else {
//Toast.makeText(getApplicationContext(), numberOfPeople+"\n"+dry+"\n"+liquid,Toast.LENGTH_LONG ).show();

Intent in1=new Intent(NumberOfPeoplePage.this, Tatale.class);
//////in1.putExtra("numberofpeople", Integer.valueOf(np.getValue()));
in1.putExtra("numberofpeople", numberOfPeople);
in1.putExtra("dry", dry);
in1.putExtra("liquid", liquid);
//Toast.makeText(getApplicationContext(),dry+ "\n"+liquid+"\n"+numberOfPeople, Toast.LENGTH_LONG).show();
startActivity(in1);
//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
NumberOfPeoplePage.this.finish();

							}
							
							
						}
						
						
						
//				
									
					
						
						
				//	}
				});
				
				

				
				
				
				
		
	}
	
////		//np.setWrapSelectorWheel(false);
//		//np.setOnValueChangedListener(this);
//		b1.setOnClickListener(new OnClickListener() {
//			

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		startActivity(new Intent(getApplicationContext(),Appetizer.class));
		//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
		
		NumberOfPeoplePage.this.finish();
	}
	
	}



//}