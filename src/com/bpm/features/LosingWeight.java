package com.bpm.features;

import java.util.ArrayList;
import java.util.Random;

import com.bpm.ghkitchen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LosingWeight extends Activity{
TextView breaktv,lunchtv,suppertv,breakfastgiventv,lunchgiventv,suppergiventv,notice;
private ArrayList<String>breakfastlist;
private ArrayList<String>lunchlist;
private ArrayList<String>supperlist;
Button btneat;

Random random = new Random();

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.losingweight);
		breaktv=(TextView) findViewById(R.id.breakfasttv);
		lunchtv=(TextView) findViewById(R.id.lunchtv);
		suppertv=(TextView) findViewById(R.id.suppertv);
		breakfastgiventv=(TextView) findViewById(R.id.breakfastgiventv);
		lunchgiventv=(TextView) findViewById(R.id.lunchgiventv);
		suppergiventv=(TextView) findViewById(R.id.suppergiventv);
		notice=(TextView) findViewById(R.id.noticetv);
		btneat=(Button) findViewById(R.id.mealbut);

		
		breakfastlist=new ArrayList<String>();
		lunchlist=new ArrayList<String>();
		supperlist=new ArrayList<String>();
		//BREAK FAST
		breakfastlist.add("HOT GREEN TEA AND BREAD");
		breakfastlist.add("LIPTON TEA & BREAD");
		breakfastlist.add("LOW FAT CREAM CHEESE WITH WHEAT BREAD & ORANGE JUICE");
		breakfastlist.add("WHEAT PORRIDGE AND WHEAT BREAD");
		
		breakfastlist.add("COCOA TEA & BREAD");
		breakfastlist.add("CORN FLAKES WITH MILKSHAKE");
		breakfastlist.add("COFFEE & BREAD");
		breakfastlist.add("LEMON JUICE & HONEY WITH BREAD");
		breakfastlist.add("SKIMMED MILK & BREAD");
		breakfastlist.add("SCRAMBLED EGGS & SMOKED SALMON");
		breakfastlist.add("TOM BROWN & BREAD");
		breakfastlist.add("OATMEAL & 2 SLICES OF BREAD");
		breakfastlist.add("HAUSA KOKO & BUFF LOAF");
		breakfastlist.add("GREEN TEA & BREAD & JAM");
		breakfastlist.add("GREEN TEA & BREAD & MAGARINE");
		breakfastlist.add("LIPTON TEA & BREAD & PEANUT BUTTER");
		breakfastlist.add("COCOA TEA & BISCUITS");
		breakfastlist.add("RICE PUDDING & BREAD & MAGARINE");
		breakfastlist.add("CUSTARD PUDDING & BREAD");
		breakfastlist.add("TOM BROWN & BREAD & JAM");
		breakfastlist.add("MILO & BREAD & JAM");
		//LUNCH LIST
		lunchlist.add("2 LADDLES OF RICE & KONTOMIRE STEW");
		lunchlist.add("3 FINGERS OF PLANTAIN  & GRAVY");
		lunchlist.add("2 SLICES OF YAM & KONTOMIRE");
		lunchlist.add("3 LADDLES OF RICE & GRAVY & MEAT");
		lunchlist.add("3 FINGERS OF COOKED UNRIPED PLANTAIN(APIM) & VEGETABLE STEW");
		lunchlist.add("1 GARI BALL(EBA) & GROUNDED PEPPER & FISH");
		lunchlist.add("2 LADDLES OF RICE & FISH STEW");
		lunchlist.add("1 BALL OF BANKU & GROUNDNUT SOUP");
		lunchlist.add("1 BALL OF BANKU & LIGHT SOUP");
		lunchlist.add("WHITE RICE & LIGHT SOUP");
		lunchlist.add("WHITE RICE & LEAN STEW");
		lunchlist.add("NONFAT VANILLA OR LEMON YOGHURT");
		lunchlist.add("ROASTED PLANTAIN & GROUNDNUT");
		
		lunchlist.add("1 BALL OF BANKU & OKRO STEW");
		
                   //SUPPER
    	supperlist.add("VEGETABLE SOUP");
		supperlist.add("VEGETABLE SALAD ");
		supperlist.add("FRUIT SALAD");
		supperlist.add("CHOPPED MELONS");
		supperlist.add("MILKSHAKE");
		supperlist.add("BANANA");
		supperlist.add("LIGHT CHICKEN SOUP");
		supperlist.add("GREEN TEA & BRAN BREAD");
		supperlist.add("LEMON TEA");
		supperlist.add("BROWN RICE & STEW");
		supperlist.add("APPLES");
		supperlist.add("BROWN RICE & MUSHROOM STEW");
		supperlist.add("GRAPES");      	    	
		supperlist.add("LEMON JUICE");      	    	
		supperlist.add("CHOPPED MANGOES");
		supperlist.add("GARLIC JUICE");      	    	
		supperlist.add("GARLIC-ROASTED LEAN PORK");      	    	
		
		
		
		btneat.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				String derived=breakfastlist.get(random.nextInt(breakfastlist.size()));
				Log.d("   ", derived);
//					
				breakfastgiventv.setText(derived);
				
				String derived1=lunchlist.get(random.nextInt(lunchlist.size()));
				Log.d("  ",derived1);
				
				lunchgiventv.setText(derived1);
				
				String derived2=supperlist.get(random.nextInt(supperlist.size()));
				Log.d("  ",derived2);
				suppergiventv.setText(derived2); 
				
				
			}
		});
		
		
		
		
		
	}
	
	

@Override
public void onBackPressed() {
	startActivity(new Intent(LosingWeight.this, PlannerPage.class));
	//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
	LosingWeight.this.finish();
	
}



}
