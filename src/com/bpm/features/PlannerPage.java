package com.bpm.features;

import java.util.ArrayList;

import com.blo.classes.MainListView;
import com.bpm.ghkitchen.R;
//import com.bpm.ghkitchen.Appetizer.MyGridWeatherAdaptor;
//import com.bpm.ghkitchen.Appetizer.MyGridWeatherAdaptor;
import com.bpm.ghkitchen.R.drawable;
import com.bpm.ghkitchen.R.id;
import com.bpm.ghkitchen.R.layout;
import com.bpm.ghkitchen.R.menu;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class PlannerPage extends ActionBarActivity {
	GridView gv;
	AlertDialog.Builder builder;
	ArrayList<MainListView> photos = new ArrayList<MainListView>();
	TextView tv;
	
//	photos.add(new MainListView("Weight Loss", R.drawable.loss));
//	photos.add(new MainListView("Weight Maintenance", R.drawable.maintanence));
//	photos.add(new MainListView("Weight Gain", R.drawable.overweight));
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_planner1);
//		tv=(TextView) findViewById(R.id.plannertv);

		gv = (GridView) findViewById(R.id.weathergrid);
ArrayList<MainListView> photos = new ArrayList<MainListView>();
//	
photos.add(new MainListView("Weight Loss", R.drawable.loss));
photos.add(new MainListView("Weight Maintenance", R.drawable.maintanence));
photos.add(new MainListView("Weight Gain", R.drawable.overweight));

//		
MyGridWeatherAdaptor gad = new MyGridWeatherAdaptor(this, photos);
gv.setAdapter(gad);	

gv.setOnItemClickListener(new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		MainListView mem=(MainListView)parent.getItemAtPosition(position);
		final String selectName=mem.getName();
		Log.d("WATCH THIS", selectName);
		
		if(selectName.equals("Weight Loss")){
			startActivity(new Intent(getApplicationContext(), LosingWeight.class));
//			//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			PlannerPage.this.finish();
		}
		else if(selectName.equals("Weight Maintenance")){
			startActivity(new Intent(getApplicationContext(), MaintainingWeight.class));
//			//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			PlannerPage.this.finish();
		}
else if(selectName.equals("Weight Gain")){
			
		}
else{
Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
}
		
		
	}
} );
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.planner1, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	

class MyGridWeatherAdaptor extends BaseAdapter
{
	Context ctx;
	ArrayList<MainListView> ctrys;
	
	public MyGridWeatherAdaptor(Context ctx, ArrayList<MainListView> ctrys) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.ctrys = ctrys;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ctrys.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ctrys.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MainListView ctry = ctrys.get(position);
		LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
	//	View v = linf.inflate(R.layout.gridview, null);
		convertView = linf.inflate(R.layout.gridview, null);
		
		ImageView photo = (ImageView) convertView.findViewById(R.id.pp);
	//	TextView details = (TextView) v.findViewById(R.id.weatherinfo);
		TextView name = (TextView) convertView.findViewById(R.id.stdinfo);
		photo.setImageResource(ctry.getImage());
		//details.setText(ctry.getTemp()+"*C");
		name.setText(ctry.getName());
		
		return convertView;

	}	
}


@Override
public void onBackPressed() {
	startActivity(new Intent(PlannerPage.this, HealthPage.class));
	//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
	PlannerPage.this.finish();
	
}


	
	
}
