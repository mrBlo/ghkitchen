package com.bpm.features;

import java.util.ArrayList;

import com.blo.classes.MainListView;
import com.bpm.ghkitchen.HomePage;
import com.bpm.ghkitchen.IngredientSubstitute;
import com.bpm.ghkitchen.R;
import com.bpm.tips.Tips;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Settings extends Activity{
	ListView lsv;
	ArrayList<MainListView> allstds = new ArrayList<MainListView>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingspage);
		lsv = (ListView) findViewById(R.id.frlist);
		allstds.add(new MainListView("Ingredient Substitute", R.drawable.ingredientsub));
				//allstds.add(new MainListView("Meal Planner", R.drawable.smoothies));
		
		yoyo stad = new yoyo(this, allstds);
		lsv.setAdapter(stad);
		
		lsv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				
				// TODO Auto-generated method stub
				MainListView mem=(MainListView)parent.getItemAtPosition(position);
				final String selectName=mem.getName();
				Log.d("WATCH THIS", selectName);
				if(selectName.equals("Ingredient Substitute")){
					startActivity(new Intent(getApplicationContext(), IngredientSubstitute.class));
					Settings.this.finish();
					}

			
				
//	else if(selectName.equals("About Us")){
//		startActivity(new Intent(getApplicationContext(), AboutUs.class));
//	}
//				
//	else if(selectName.equals("Health Tips")){
//		startActivity(new Intent(getApplicationContext(), HealthPage.class));
//	}
//				
				
				
				
else{
	Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
}
				
				
			}
		});

		

		
		
	}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			//super.onBackPressed();
			startActivity(new Intent(getApplicationContext(), HomePage.class));
			//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
			Settings.this.finish();
		}
		}

	class yoyo extends BaseAdapter
	{
		Context ctx;
		ArrayList<MainListView> allstd;
		
		public yoyo(Context ctx, ArrayList<MainListView> stds)
		{
			this.ctx = ctx;
			allstd = stds;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			
			return allstd.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stublist.get(i);
			return allstd.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
		
		
	//NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
		@Override
		public View getView(int index, View convertView, ViewGroup parent) {
			//NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
			//GETTING THE STUDENT OBJECT AT THAT INDEX
			MainListView st = allstd.get(index);
			
			
			// TODO Auto-generated method stub
					//Requesting an inflater from system to dynamically inflate a layout with contents
			LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
					//inflate fuction returns a view reference to the layout to be inflated
			View v = linf.inflate(R.layout.listview, null);
			
			
					//NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
			ImageView photo = (ImageView) v.findViewById(R.id.pp);
			TextView details = (TextView) v.findViewById(R.id.stdinfo);
			
					//now time to inflate the views on LAYOUT V
			photo.setImageResource(st.getImage());
			details.setText(st.toString());
			
			return v;
		}



	

	
	}
	

