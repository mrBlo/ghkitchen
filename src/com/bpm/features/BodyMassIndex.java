package com.bpm.features;

//import com.blo.kitchenGh.R;
//import com.blo.kitchenGh.R.layout;

import com.bpm.ghkitchen.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BodyMassIndex extends Activity {
TextView definitiontv,cattv,weighttv,heighttv,answerBMI;
EditText weightET,heightET;
float weight,height,bmi;
String weight1,height1,bmi1;
Button calc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_body_mass_index);
//		definitiontv=(TextView) findViewById(R.id.bmitv);
//		cattv=(TextView) findViewById(R.id.bmitv2);
//		weighttv=(TextView) findViewById(R.id.bmitv3);
//		heighttv=(TextView) findViewById(R.id.bmitv4);
	answerBMI=(TextView) findViewById(R.id.answerBMItextView);
//		
		weightET=(EditText) findViewById(R.id.weighteditText);
		heightET=(EditText) findViewById(R.id.HeighteditText);
		calc= (Button) findViewById(R.id.calculatebutton);
		
		
		
//		
////		weight1=weightET.getText().toString();
////		height1=heightET.getText().toString();
////		
//		
		calc.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
		//	Toast.makeText(getApplicationContext(), "We will do this later", Toast.LENGTH_LONG).show();
	//HIDING KEYBOARD
				InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	imm.hideSoftInputFromWindow(calc.getWindowToken(), 0);
			
			if(weightET.getText().toString().isEmpty()|| heightET.getText().toString().isEmpty()){
				Toast.makeText(BodyMassIndex.this, "Please fill all boxes",Toast.LENGTH_LONG).show();
			}
			else {
				weight=Float.parseFloat(weightET.getText().toString());
				height= Float.parseFloat(heightET.getText().toString());
				
				bmi= weight/(height*height);
				if (bmi<10) {
					answerBMI.setText("Your Body Mass Index is below 5,\nit can't be that small");
				}
				
				else if (bmi>=10 && bmi<18.5) {
					answerBMI.setText("Your Body Mass Index is "+bmi+",\nYou are probably underweight");
				}
				else if (bmi>=18.5 && bmi<25) {
					answerBMI.setText("Your Body Mass Index is "+bmi+",\nYou have a normal weight");
				}
				else if (bmi>=25 && bmi<30) {
					answerBMI.setText("Your Body Mass Index is "+bmi+",\nYou are probably overweight\nPlease check your diet");
				}
			
				else {
					answerBMI.setText("Your Body Mass Index is "+bmi+"\nCheck your diet and exercise");
				}
				
				weightET.setText("");
				heightET.setText("");
			}
			
			
			
		//	bmi=100/height*weight;
			
			}
		});
		
		
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		startActivity(new Intent(getApplicationContext(), HealthPage.class));
		//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
		BodyMassIndex.this.finish();
	}
	
}
