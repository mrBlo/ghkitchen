package com.bpm.features;

import java.util.ArrayList;
import java.util.Random;

import com.bpm.ghkitchen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MaintainingWeight extends Activity{
TextView breaktv,lunchtv,suppertv,breakfastgiventv,lunchgiventv,suppergiventv,notice;
private ArrayList<String>breakfastlist;
private ArrayList<String>lunchlist;
private ArrayList<String>supperlist;
Button btneat;

Random random = new Random();

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maintainingweight);
		breaktv=(TextView) findViewById(R.id.breakfasttv);
		lunchtv=(TextView) findViewById(R.id.lunchtv);
		suppertv=(TextView) findViewById(R.id.suppertv);
		breakfastgiventv=(TextView) findViewById(R.id.breakfastgiventv);
		lunchgiventv=(TextView) findViewById(R.id.lunchgiventv);
		suppergiventv=(TextView) findViewById(R.id.suppergiventv);
		notice=(TextView) findViewById(R.id.noticetv);
		btneat=(Button) findViewById(R.id.mealbut);

		
		breakfastlist=new ArrayList<String>();
		lunchlist=new ArrayList<String>();
		supperlist=new ArrayList<String>();
		//BREAK FAST
		breakfastlist.add("TEA AND BREAD");
		//breakfastlist.add("KOOKO & BREAD");
		breakfastlist.add("CREAM CHEESE WITH WHEAT BREAD & TEA");
		breakfastlist.add("WHEAT PORRIDGE AND WHEAT BREAD");
		
		breakfastlist.add("COCOA TEA & BREAD");
		breakfastlist.add("CORN FLAKES WITH MILKSHAKE");
		breakfastlist.add("COFFEE & BREAD");
		breakfastlist.add("WAAKYE,SALAD AND ONE EGG");
		breakfastlist.add("ORANGE JUICE AND TOAST");
		breakfastlist.add("SCRAMBLED EGGS & TUNA");
		breakfastlist.add("TOM BROWN & BREAD");
		breakfastlist.add("OATMEAL & 4 SLICES OF BREAD");
		breakfastlist.add("HAUSA KOKO & BUFF LOAF");
		breakfastlist.add("COCO POPS AND BREAD WITH CHOCOLATE SPREAD");
		breakfastlist.add("RICE CRISPIES");
		breakfastlist.add("TEA LEAF & BREAD & PEANUT BUTTER");
		breakfastlist.add("COCOA TEA & BISCUITS WITH CHOCOLATE SPREAD");
		breakfastlist.add("RICE PUDDING & BREAD & MAGARINE");
		breakfastlist.add("CUSTARD PUDDING & ICE CREAM");
		breakfastlist.add("CUSTARD PUDDING & DIARY MILK");
		breakfastlist.add("TOM BROWN & BREAD & JAM");
		breakfastlist.add("MILO & BREAD & JAM");
		//LUNCH LIST
		lunchlist.add("RICE & KONTOMIRE STEW");
		lunchlist.add("RIPED PLANTAIN  & GRAVY");
		lunchlist.add("BOILED YAM & KONTOMIRE STEW");
		lunchlist.add("RICE & GRAVY & MEAT");
		lunchlist.add("COOKED UNRIPED PLANTAIN(APIM) & VEGETABLE STEW");
		lunchlist.add("1 GARI BALL(EBA) & GROUNDED PEPPER & FISH");
		lunchlist.add("RICE & FISH STEW");
		lunchlist.add("BANKU & GROUNDNUT SOUP");
		lunchlist.add("BANKU & PALM NUT SOUP");
		lunchlist.add("BROWN RICE & LIGHT SOUP");
		lunchlist.add("WHITE RICE & TOMATO STEW");
		lunchlist.add("FUFU & GROUNDNUT SOUP");
		lunchlist.add("FUFU & PALM NUT SOUP");
		lunchlist.add("FUFU & LIGHT SOUP");
		lunchlist.add("BANKU & OKRO STEW");
		lunchlist.add("BANKU & PEPPER WITH TILAPIA");
		lunchlist.add("OMO TUO & GROUNDNUT SOUP");
		lunchlist.add("OMO TUO & PALM NUT SOUP");
		
		
		
                   //SUPPER
    	supperlist.add("KENKEY & PEPPER WITH FRIED FISH");
		supperlist.add("KENKEY & PEPPER WITH TUNA ");
		supperlist.add("KENKEY & OKRO STEW");
		supperlist.add("CHOPPED MELONS");
		supperlist.add("RICE & KONTOMIRE STEW");
		supperlist.add("FRUIT SALAD");
		supperlist.add("BOILED YAW & LIGHT CHICKEN SOUP");
		supperlist.add("MASHED PLANTAIN WITH GROUNDNUT & SLICED EGGS(3TO)");
		supperlist.add("LEMON TEA & TOAST");
		supperlist.add("BROWN RICE & STEW");
		supperlist.add("SLICED APPLES & GRAPES");
		supperlist.add("BROWN RICE & MUSHROOM STEW");
		supperlist.add("KELEWELE");      	    	
		supperlist.add("JOLLOF RICE");      	    	
		supperlist.add("FRIED RICE & CHICKEN");
		supperlist.add("POTATO CHIPS & CHICKEN");      	    	
		supperlist.add("FRIES & GARLIC-ROASTED LEAN PORK");      	    	
		
		
		
		btneat.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				String derived=breakfastlist.get(random.nextInt(breakfastlist.size()));
				Log.d("   ", derived);
//					
				breakfastgiventv.setText(derived);
				
				String derived1=lunchlist.get(random.nextInt(lunchlist.size()));
				Log.d("  ",derived1);
				
				lunchgiventv.setText(derived1);
				
				String derived2=supperlist.get(random.nextInt(supperlist.size()));
				Log.d("  ",derived2);
				suppergiventv.setText(derived2); 
				
				
			}
		});
		
		
		
		
		
	}
	
	

@Override
public void onBackPressed() {
	startActivity(new Intent(MaintainingWeight.this, PlannerPage.class));
	//overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
	MaintainingWeight.this.finish();
	
}



}
