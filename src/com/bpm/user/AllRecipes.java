package com.bpm.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.blo.classes.MainListView;
//import org.proj.bus.AddBus;
//import org.proj.bus.AllBuses;
//import org.proj.bus.UpdateBus;
//import org.proj.bus.AllBuses.LoadAllBuses;
//
//import com.blo.classes.Bus;
import com.blo.classes.Recipe;

//import com.blo.gson.JSONParser;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AllRecipes extends ListActivity {
	String username,password;
	
	ListView lv;

	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON parser object
	JSONParser jParser = new JSONParser();

	ArrayList<HashMap<String, String>> BusList;
	//ArrayList<String> busNumbers;
	ArrayList<Recipe> busNumbers;

	// url to get all products list
	private static String url_all_Buses = "http://172.18.137.124/ghkitchen/all.php";

	// JSON Node names
	public static final String TAG_SUCCESS = "success";
	public static final String TAG_BUSES = "recipes";
	public static final String TAG_BUSID = "username";
	public static final String TAG_BUSMODEL = "name";
	public static final String TAG_BUS_NO = "ingredients";
	public static final String TAG_BUS_DEST="method";
	ArrayAdapter<HashMap<String, String>> adapter;

	
	Recipe recipe;
	
	// buses JSONArray
	JSONArray recipes = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.viewbuses);
		
		recipe=new Recipe();

		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");
		
		// Get listview
		lv = getListView();
	//	busNumbers=new ArrayList<String>();

		// Hashmap for ListView
		BusList = new ArrayList<HashMap<String, String>>();

		// Loading buses in Background Thread
		
		new LoadAllBuses().execute();
		adapter = new ArrayAdapter<HashMap<String, String>>(
				AllRecipes.this, android.R.layout.simple_list_item_1,
				BusList);
		lv.setAdapter(adapter);
		// on selecting single bus
		// launching Edit Bus Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				//MainListView mem=(MainListView)parent.getItemAtPosition(position);
//				final String selectName=mem.getName();
//				Log.d("WATCH THIS", selectName);
//				if(selectName.equals(username)){
				
				
			Recipe selectedBus=(Recipe)	parent.getItemAtPosition(position);
				Toast.makeText(AllRecipes.this, "you selected "+selectedBus+"\n"+"Please wait...", Toast.LENGTH_LONG).show();
				// TODO Auto-generated method stub
		//		Toast.makeText(AllRecipes.this, "you selected ..."+username1"+"\n"+"name1"+"\ningredients1"+"\nmethod1", Toast.LENGTH_LONG).show();
				
				// getting values from selected ListItem
				
				Intent in = new
				 Intent(getApplicationContext(),SelectedRecipe.class);

				// sending bus_id to next activity
//  i.putExtra("username",username);
				
				
			
				
				
				
//				Bundle bundle=new Bundle();
//				 bundle.putSerializable("bus",recipe);
//				  in.putExtra("username",username);
//				  in.putExtra("password",password);
				Toast.makeText(getApplicationContext(),recipe.toString(), Toast.LENGTH_LONG).show();
				
//				  in.putExtra("username1",recipe.getUsername());
//				  in.putExtra("name1",recipe.getName());
//				  in.putExtra("ingredients1",recipe.getIngredients());
//				  in.putExtra("method1",recipe.getProcedures());
//					
			//	 in.putExtras(bundle);

//				 startActivity(in);
//				AllRecipes.this.finish();

			}
		});

	}

	

	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class LoadAllBuses extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AllRecipes.this);
			pDialog.setMessage("Loading Recipes. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting All buses from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_Buses, "GET",
					params);

			// Check your log cat for JSON response
			Log.d("All Buses: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success=1;// = json.getInt(TAG_SUCCESS);
				Log.d("REturned success tag", success + "");
				if (success == 1) {
					Log.d("within success tag", success + "");
					// products found
					// Getting Array of Buses
					recipes = json.getJSONArray(TAG_BUSES);
					Log.d("Returned buses", recipes + "");
					// looping through All Buses
					for (int i = 0; i < recipes.length(); i++) {
						JSONObject c = recipes.getJSONObject(i);
						Log.d("each recipe", c + "");
						// Storing each json item in variable
						String username = c.getString(TAG_BUSID);

						Log.d("bus id", username + "");
						
						String ingredients = c.getString(TAG_BUS_NO);
						Log.d("ingredients", ingredients + "");
						
						String name = c.getString(TAG_BUSMODEL);
						Log.d("name", name + "");
						
						
						String method = c.getString(TAG_BUS_DEST);
						Log.d("method", method + "");
						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_BUSMODEL, name);
						map.put(TAG_BUSID, username);
						
//						// adding HashList to ArrayList
//						BusList.add(map);
//						busNumbers.add(name+"\n"+
//						"Posted by "+username);
							
							recipe.setName(name);
							recipe.setUsername(username);
							recipe.setIngredients(ingredients);
							recipe.setProcedures(method);
						//}
							
							// adding HashList to ArrayList
							BusList.add(map);
							busNumbers.add(recipe);
							
							
					}
					

				} else {
					// no buses found
					// Launch Add New product Activity
					Toast.makeText(getApplicationContext(), "No Recipes have been posted", Toast.LENGTH_LONG).show();
//					Intent i = new Intent(getApplicationContext(), AddBus.class);

					// Closing all previous activities
//					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					startActivity(i);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
				
					ArrayAdapter<Recipe> cad = new ArrayAdapter<Recipe>(
							AllRecipes.this, android.R.layout.simple_list_item_1,busNumbers);
					lv.setAdapter(cad);

				}
			});

		}
	}
	

	@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			//super.onBackPressed();
		 Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
		  i.putExtra("username",username);
		  i.putExtra("password",password);
		  startActivity(i);
		  finish();
		}
	
	
}
