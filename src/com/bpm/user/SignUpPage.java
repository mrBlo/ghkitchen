package com.bpm.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.bpm.ghkitchen.HomePage;
import com.bpm.ghkitchen.R;
//import com.example.phonebook.Signin.loginAccess;
//import com.example.phonebook.Signin;
//import com.example.phonebook.Welcome;
////import com.example.phonebook.JSONParser;
//import com.example.phonebook.Signin;
//import com.example.phonebook.Signin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpPage extends Activity{
	Button signup;
	String email,username,password;
	EditText emailet,usernameet,passwordet;
	
	private ProgressDialog pDialog;
	int flag=0;
	JSONParser jsonParser = new JSONParser();
	//10.0.2.2
	//   C:\xampp\htdocs\ghkitchen	http://10.0.2.2/rdias/register.php	10.0.2.2/xampp/htdocs
	private static String url = "http://172.18.27.62/ghkitchen/register.php";
	private static final String TAG_SUCCESS = "success"; 
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.signup);
	
	StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
	.detectDiskReads().detectDiskWrites().detectNetwork()
	.penaltyLog().build());
	
	passwordet= (EditText) findViewById(R.id.Passet);
	emailet=(EditText) findViewById(R.id.Emailet);
	usernameet= (EditText) findViewById(R.id.UserNameet);

	signup= (Button) findViewById(R.id.btnSignUp);
	
	
	email=emailet.getText().toString();
	username=usernameet.getText().toString();
	password=passwordet.getText().toString();
	
	signup.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			
			if (emailet.getText().toString().isEmpty()||usernameet.getText().toString().isEmpty()||passwordet.getText().toString().isEmpty()) {
				
				Toast.makeText(getApplicationContext(), "fill all boxes", Toast.LENGTH_LONG).show();
				return;
			}
			
			 if(passwordet.getText().toString().length()<4)
				{				
					Toast.makeText(SignUpPage.this,"Please Enter minimum 4 letters in password", Toast.LENGTH_LONG).show();
					return;
				}
			
			
				 if(!isOnline(SignUpPage.this))
					{					
						Toast.makeText(SignUpPage.this,"No network connection", Toast.LENGTH_LONG).show();
						return;	
						
					}	
					
				//executes the async
				 new loginAccess().execute();
			}
		//}
	});
	
	
}
	
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	SignUpPage.this.finish();
startActivity(new Intent(getApplicationContext(), UserPage1.class));		
}	


//code to check online details
private boolean isOnline(Context mContext) {
ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
NetworkInfo netInfo = cm.getActiveNetworkInfo();
if (netInfo != null && netInfo.isConnectedOrConnecting())
	  {
	return true;
		}
    return false;
	}



class loginAccess extends AsyncTask<String, String, String> {

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(SignUpPage.this);
		pDialog.setMessage("Registering...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}
	@Override
	protected String doInBackground(String... arg0) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String username=usernameet.getText().toString();
		String email=emailet.getText().toString();
		String password=passwordet.getText().toString();
//		String h=hint.getText().toString();
//		String id=email_id.getText().toString();
//		
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("password", password));
		//params.add(new BasicNameValuePair("email_id", id));
		
		JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
		
		Log.d("Create Response", json.toString());
		
		try {
			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			 {
			  flag=0;	
			  Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
			  i.putExtra("username",username);
			  i.putExtra("password",password);
			  startActivity(i);
			  finish();
			 }
			 else
			 {
				// failed to Sign in
				flag=1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		if(flag==1)
			Toast.makeText(SignUpPage.this,"Invalid ID" +
					"\n" +
					"Your username is unavailable" +
					"\n" +
					"Please use another ID", Toast.LENGTH_LONG).show();
		
	}
	
  }





}
