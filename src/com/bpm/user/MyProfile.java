package com.bpm.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.blo.classes.User;
import com.bpm.ghkitchen.R;
import com.bpm.ghkitchen.R.id;
import com.bpm.ghkitchen.R.layout;
//import com.bpm.user.SignInPage.loginAccess;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MyProfile extends Activity {

	Button update;
	String username,password,freshusername,freshpassword,oldusername;
	EditText usernameEt,passwordEt;
	TextView oldusernameTV;
	
	//192.168.173.1
	private static String url_all_Buses = "http://172.18.27.14/ghkitchen/update_user.php";
	private ProgressDialog pDialog;

	JSONParser jParser = new JSONParser();
	JSONArray buses = null;

	ArrayList<HashMap<String, String>> BusList;
	ArrayList<String> busNumbers;
	
	//String username = "";
	User bus;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myprofile);
		
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		.detectDiskReads().detectDiskWrites().detectNetwork()
		.penaltyLog().build());
		
		
		oldusername=getIntent().getStringExtra("username"); 
		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");
		
update= (Button) findViewById(R.id.editUser);
		
oldusernameTV=(TextView) findViewById(R.id.oldusernametextView);
		passwordEt= (EditText) findViewById(R.id.editpasswordEditText);
		usernameEt= (EditText) findViewById(R.id.editusernameEditText);
		
		//setting old credentials
		oldusernameTV.setText(oldusername);
		usernameEt.setText(username);
		passwordEt.setText(password);
		
update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 freshusername = usernameEt.getText().toString();
				 freshpassword = passwordEt.getText().toString();
			
				
				if (usernameEt.getText().toString().isEmpty()||passwordEt.getText().toString().isEmpty()) {
					
					Toast.makeText(getApplicationContext(), "fill all boxes", Toast.LENGTH_LONG).show();
					return;
				}
				
				 if(passwordEt.getText().toString().length()<4)
					{				
						Toast.makeText(MyProfile.this,"Please Enter minimum 4 letters in password", Toast.LENGTH_LONG).show();
						return;
					}
				
				
					 if(!isOnline(MyProfile.this))
						{					
							Toast.makeText(MyProfile.this,"No network connection", Toast.LENGTH_LONG).show();
							return;	
							
						}	

					 
					 
						new UpdateBusData().execute();
					 
					//	clearFields();

						 Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
						  i.putExtra("username",freshusername);
						  i.putExtra("password",freshpassword);
						  startActivity(i);
						  MyProfile.this.finish();
					// Toast.makeText(getApplicationContext(), "Account Updated", Toast.LENGTH_LONG).show();
					//	new loginAccess().execute();
			}
			//code to check online details
			
			
			
			
			private boolean isOnline(Context mContext) {
			ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting())
		   	  {
				return true;
	     		}
			    return false;
	       	}
	      //Close code that check online details		
		  }); 
	        //Close log in 
		
		

		
		
	}





	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		 Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
		  i.putExtra("username",freshusername);
		  i.putExtra("password",freshpassword);
		  startActivity(i);
		  MyProfile.this.finish();
}
	private void clearFields(){
		//Et2.setText("");
		usernameEt.setText("");
		passwordEt.setText("");
	}

	private class UpdateBusData extends AsyncTask<String, Integer, String> {

		protected String doInBackground(String... args) {
			
			String oldusername1 = oldusernameTV.getText().toString();
			
			String freshusername1 = usernameEt.getText().toString();
			String freshpassword1 = passwordEt.getText().toString();
			 
			
			
			
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();

//			params.add(new BasicNameValuePair("bus_id", bus_id));
			params.add(new BasicNameValuePair("oldusername", oldusername1));
			params.add(new BasicNameValuePair("username", freshusername1));
			params.add(new BasicNameValuePair("password", freshpassword1));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_Buses, "POST",
					params);

			// Check your log cat for JSON response
			Log.d("Update User: ", json.toString());

			// Checking for SUCCESS TAG
			int success =1;// json.getInt(AllBuses.TAG_SUCCESS);
			Log.d("Update User success tag", success + "");
			if (success == 1) {
				Log.d("within update success tag", success + "");
				

			} else {
				
			}

			return null;
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MyProfile.this);
			pDialog.setMessage("Updating User. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}
		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					

				}
			});

		}


}
	}
