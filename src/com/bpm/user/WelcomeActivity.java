package com.bpm.user;

import java.util.ArrayList;

import com.blo.classes.MainListView;
import com.bpm.features.HealthPage;
import com.bpm.features.Settings;
import com.bpm.ghkitchen.AfricanPage;
import com.bpm.ghkitchen.HomePage;
import com.bpm.ghkitchen.R;
//import com.bpm.ghkitchen.HomePage.MyGridWeatherAdaptor;
import com.bpm.ghkitchen.R.layout;
//import com.example.phonebook.JSONParser;
//import com.example.phonebook.Welcome.loginAccess;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class WelcomeActivity extends Activity {

//	private ProgressDialog pDialog;
//	int flag=0;
//	JSONParser jsonParser = new JSONParser();
//	private static String url = "http://10.0.2.2/rdias/about.php";
//	private static final String about = "about"; 
//	private static final String TAG_SUCCESS="success";
	
	String username,password;
	GridView gv;
	AlertDialog.Builder builder;
	ArrayList<MainListView> photos = new ArrayList<MainListView>();
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//		.detectDiskReads().detectDiskWrites().detectNetwork()
//		.penaltyLog().build());
		

		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome);

		gv = (GridView) findViewById(R.id.weathergrid);
		
		Toast.makeText(getApplicationContext(), "Welcome "+username, Toast.LENGTH_LONG).show();
		
		ArrayList<MainListView> photos = new ArrayList<MainListView>();
		photos.add(new MainListView(username, R.drawable.blanprofile));
		photos.add(new MainListView("My Recipes", R.drawable.myrecipenew));
		photos.add(new MainListView("Trending Recipes", R.drawable.buffettwo));
	



//
		MyGridWeatherAdaptor gad = new MyGridWeatherAdaptor(this, photos);
		gv.setAdapter(gad);	
gv.setOnItemClickListener(new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		MainListView mem=(MainListView)parent.getItemAtPosition(position);
		final String selectName=mem.getName();
		Log.d("WATCH THIS", selectName);
		if(selectName.equals(username)){
			//Toast.makeText(getApplicationContext(), "Selected Input", Toast.LENGTH_LONG).show();
//		startActivity(new Intent(HomePage.this, AfricanPage.class));
////		//	overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//		HomePage.this.finish();	
			 Intent i = new Intent(getApplicationContext(),MyProfile.class);
			  i.putExtra("username",username);
			  i.putExtra("password",password);
			  startActivity(i);
			  finish();
		}
	
		else if(selectName.equals("My Recipes")){
			 Intent i = new Intent(getApplicationContext(),MyRecipesPage.class);
			  i.putExtra("username",username);
			  i.putExtra("password",password);
			  startActivity(i);
			  finish();}
		
else if(selectName.equals("Trending Recipes")){
	 Intent i = new Intent(getApplicationContext(),AllRecipes.class);
	  i.putExtra("username",username);
	  i.putExtra("password",password);
	  startActivity(i);
	  finish();	}

		
		
		else{
			Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
		}
		
		
	}
} );
		
	}
	
	

class MyGridWeatherAdaptor extends BaseAdapter
{
	Context ctx;
	ArrayList<MainListView> ctrys;
	
	public MyGridWeatherAdaptor(Context ctx, ArrayList<MainListView> ctrys) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.ctrys = ctrys;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return ctrys.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return ctrys.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		MainListView ctry = ctrys.get(position);
		LayoutInflater linf = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
	//	View v = linf.inflate(R.layout.gridview, null);
		convertView = linf.inflate(R.layout.gridview, null);
		
		ImageView photo = (ImageView) convertView.findViewById(R.id.pp);
	//	TextView details = (TextView) v.findViewById(R.id.weatherinfo);
		TextView name = (TextView) convertView.findViewById(R.id.stdinfo);
		photo.setImageResource(ctry.getImage());
		//details.setText(ctry.getTemp()+"*C");
		name.setText(ctry.getName());
		
		return convertView;
	}

	
	}	
		
	
	
	


	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
	startActivity(new Intent(getApplicationContext(), UserPage1.class));
	WelcomeActivity.this.finish();
	}
	
}
