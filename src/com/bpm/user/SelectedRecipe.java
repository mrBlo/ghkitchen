package com.bpm.user;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;

//import com.blo.classes.Bus;
//import com.blo.classes.Bus;
import com.blo.classes.Recipe;
//import com.blo.gson.JSONParser;
import com.bpm.ghkitchen.R;
import com.bpm.ghkitchen.R.layout;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SelectedRecipe extends Activity {
	
	String username,password;
	//private static String url_all_Buses = "http://172.18.27.183/ghkitchen/all.php";
	//private ProgressDialog pDialog;

	// Creating JSON parser object
//	JSONParser jParser = new JSONParser();
//	JSONArray buses = null;

//	ArrayList<HashMap<String, String>> BusList;
//	ArrayList<String> busNumbers;
	//Button btn;
	// Bus_managers cmg;
	TextView
	Et1,Et2,Et3,ET4;

	Intent intent;
	String username1 = "";
	Recipe bus;
	String name1 ;
	String ingredients1 ;
	String method1;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selectedrecipe);
		Et1=(TextView) findViewById(R.id.textView1);
		Et2=(TextView) findViewById(R.id.textView2);
		Et3=(TextView) findViewById(R.id.textView3);
		ET4=(TextView) findViewById(R.id.textView4);
		
		
		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");
		
		intent = getIntent();
	//	bus_no = intent.getStringExtra(AllBuses.TAG_BUS_NO);
		bus = (Recipe) intent.getSerializableExtra("bus");
		
		username1=bus.getUsername();
		name1=bus.getName();
		ingredients1=bus.getIngredients();
		method1=bus.getProcedures();
		
//		username1=getIntent().getStringExtra("username1"); 
//		name1 = getIntent().getStringExtra("name1");
//		ingredients1 = getIntent().getStringExtra("ingredients1");
//		method1=getIntent().getStringExtra("method1");
//		
		
		Et1.setText(username1);
		Et2.setText(name1);
		Et3.setText(ingredients1);
		ET4.setText(method1);
		
	}
	
	
	

	@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			//super.onBackPressed();
		 Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
		  i.putExtra("username",username);
		  i.putExtra("password",password);
		  startActivity(i);
		  finish();
		}
	
	
}
