package com.bpm.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.bpm.ghkitchen.HomePage;
import com.bpm.ghkitchen.R;
//import com.example.phonebook.MainActivity;
//import com.example.phonebook.Welcome;
//import com.example.phonebook.MainActivity.loginAccess;
//import com.example.phonebook.JSONParser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignInPage extends Activity {

	
	Button signin;
	String username,password;
	EditText usernameEt,passwordEt;
	private ProgressDialog pDialog;
	int flag=0;
	JSONParser jsonParser = new JSONParser();
	////192.168.173.1
	private static String url = "http://172.18.137.124/ghkitchen/login.php";
	private static final String TAG_SUCCESS = "success"; 

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		.detectDiskReads().detectDiskWrites().detectNetwork()
		.penaltyLog().build());
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		passwordEt= (EditText) findViewById(R.id.etPass);
		usernameEt= (EditText) findViewById(R.id.etUserName);
		
		signin= (Button) findViewById(R.id.btnSignIn);
		
		
		username=usernameEt.getText().toString();
		password=passwordEt.getText().toString();
		
		
		signin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (usernameEt.getText().toString().isEmpty()||passwordEt.getText().toString().isEmpty()) {
					
					Toast.makeText(getApplicationContext(), "fill all boxes", Toast.LENGTH_LONG).show();
					return;
				}
				
				 if(passwordEt.getText().toString().length()<4)
					{				
						Toast.makeText(SignInPage.this,"Please Enter minimum 4 letters in password", Toast.LENGTH_LONG).show();
						return;
					}
				
				
					 if(!isOnline(SignInPage.this))
						{					
							Toast.makeText(SignInPage.this,"No network connection", Toast.LENGTH_LONG).show();
							return;	
							
						}	

						new loginAccess().execute();
			}
			
			
			//code to check online details
			private boolean isOnline(Context mContext) {
			ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting())
		   	  {
				return true;
	     		}
			    return false;
	       	}
	      //Close code that check online details		
		  }); 
	        //Close log in 
		
		
		
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		SignInPage.this.finish();
startActivity(new Intent(getApplicationContext(), UserPage1.class));		
}
	
	
	

class loginAccess extends AsyncTask<String, String, String> {

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(SignInPage.this);
		pDialog.setMessage("Login...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}
	@Override
	protected String doInBackground(String... arg0) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String username=usernameEt.getText().toString();
		String password=passwordEt.getText().toString();
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
		JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
		Log.d("Create Response", json.toString());
		
		try {
			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			 {
			  flag=0;	
			  Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
			  i.putExtra("username",username);
			  i.putExtra("password",password);
			  startActivity(i);
			  finish();
			 }
			 else
			 {
				// failed to login
				flag=1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		if(flag==1)
			Toast.makeText(SignInPage.this,"Invalid username or password" +
					"\n" +
					"Please try again", Toast.LENGTH_LONG).show();
		
	}
	
  }

//////////NOTE HERE
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

	
	
	
}
