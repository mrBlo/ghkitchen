package com.bpm.user;

import java.io.ByteArrayOutputStream;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
//import org.blo.ui.AdminPage;
//import org.blo.ui.AdminsLogin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blo.classes.User;
import com.bpm.ghkitchen.R;
import com.bpm.user.SignUpPage.loginAccess;
//import org.me.transportmanagement.R;
//import org.proj.bus.AddBus;
//import org.proj.bus.AllBuses;
//import org.proj.bus.AllBuses.LoadAllBuses;

//import com.blo.classes.Bus;
//import com.blo.gson.JSONParser;
//import org.proj.bus.AddBus;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
//import android.widget.ImageView;
//import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class AddRecipe  extends Activity{
	String username;
	String name_, method_, ingredients_, username_;
	//int image_ ;
	//int CAMERA_REQUEST = 100;
	//EDITTEXT AND SPINNER
EditText username_edit,name_edit,ingredients_edit,method_edit;
//ImageView image_iv;
Button SubmitButton;
//Bitmap photo;
//ImageButton snapshotButton;
//Progress Dialog
	private ProgressDialog pDialog;
	int flag=0;
	JSONParser jsonParser = new JSONParser();
	private static String url = "http://172.18.137.124/ghkitchen/addrecipe.php";
	private static final String TAG_SUCCESS = "success"; 
	

	
	
@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
			setContentView(R.layout.addrecipe);
		
			username = getIntent().getStringExtra("username");
			
			
			
			
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
			.detectDiskReads().detectDiskWrites().detectNetwork()
			.penaltyLog().build());
		//CASTINGS
		username_edit=(EditText) findViewById(R.id.addusername_);
		name_edit=(EditText) findViewById(R.id.name_);
		ingredients_edit=(EditText) findViewById(R.id.ingredients_);
		method_edit=(EditText) findViewById(R.id.method_);
//		image_iv=(ImageView) findViewById(R.id.image);
//		snapshotButton=(ImageButton) findViewById(R.id.snapshot);
		SubmitButton=(Button) findViewById(R.id.submit_button);
		
		//unEdittable Edittexts
		username_edit.setKeyListener(null);
		username_edit.setText(username);
//		
//		snapshotButton.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
//		        startActivityForResult(cameraIntent, CAMERA_REQUEST); 
//
//			}
//		});
				
		
			
			//SUBMIT BUTTON
			SubmitButton.setOnClickListener(new OnClickListener() {
//				long schedule_time_;
//				long date_;
//				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					//Inputs of Fields
					 name_=name_edit.getText().toString();
					 username_=username_edit.getText().toString();
					 ingredients_=ingredients_edit.getText().toString();
					method_=method_edit.getText().toString();
					
					if(name_.isEmpty() || username_.isEmpty() || ingredients_.isEmpty() || method_.isEmpty() ){
						Toast.makeText(AddRecipe.this, "FILL ALL DETAILS", Toast.LENGTH_LONG).show();
					}
					
					else if(!isOnline(AddRecipe.this))
						{					
							Toast.makeText(AddRecipe.this,"No network connection", Toast.LENGTH_LONG).show();
								
							
						}	
//						
					//executes the async
//					 new loginAccess().execute();
//					
					else{
						
					
							 new loginAccess().execute();
							 
							 
					//Toast.makeText(AddRecipe.this, date_, Toast.LENGTH_LONG).show();
//					Intent in=new Intent(AddRecipe.this, AdminPage.class);
//					startActivity(in);
					}
				}
			});
			
			
			
	}
	
//
//@Override
//protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//	// TODO Auto-generated method stub
//	super.onActivityResult(requestCode, resultCode, data);
//	if(requestCode == CAMERA_REQUEST && resultCode==RESULT_OK)
//		{
//		  photo = (Bitmap) data.getExtras().get("data"); 
//		 image_iv.setImageBitmap(photo);
//		}
//		else
//		{
//			Toast.makeText(AddRecipe.this, "PLEASE TRY AGAIN", 300).show();
//		}
//}

@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	 Intent i = new Intent(getApplicationContext(),MyRecipesPage.class);
	  i.putExtra("username",username);
	//  i.putExtra("password",password);
	  startActivity(i);
	  finish();}	


//code to check online details
private boolean isOnline(Context mContext) {
ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
NetworkInfo netInfo = cm.getActiveNetworkInfo();
if (netInfo != null && netInfo.isConnectedOrConnecting())
	  {
	return true;
		}
    return false;
	}


class loginAccess extends AsyncTask<String, String, String> {

	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(AddRecipe.this);
		pDialog.setMessage("Adding Recipe..");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();
	}
	
	
	
	@Override
	protected String doInBackground(String... arg0) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		
//		 ByteArrayOutputStream stream = new ByteArrayOutputStream();
//	        photo.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
//	        byte [] byte_arr = stream.toByteArray();
//	        String image_str = Base64.encodeToString(byte_arr, Base64.DEFAULT);
		
		
		String usernamenew=username_edit.getText().toString();
		String namenew=name_edit.getText().toString();
		String ingredientsnew=ingredients_edit.getText().toString();
		String methodnew=method_edit.getText().toString();
		
		
		params.add(new BasicNameValuePair("username", usernamenew));
		params.add(new BasicNameValuePair("name", namenew));
		params.add(new BasicNameValuePair("ingredients", ingredientsnew));
		params.add(new BasicNameValuePair("method", methodnew));
		//params.add(new BasicNameValuePair("image", image_str));
		
		
		JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
		
		Log.d("Create Response", json.toString());
		
		try {
			int success = json.getInt(TAG_SUCCESS);
			if (success == 1) 
			 {
			  flag=0;	
			  Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
			  i.putExtra("username",usernamenew);
			//  i.putExtra("password",password);
			  startActivity(i);
			  finish();
			 }
			 else
			 {
				// failed to Sign in
				flag=1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		if(flag==1)
			Toast.makeText(AddRecipe.this,
					"Unable to add recipe", Toast.LENGTH_LONG).show();
		
	}
	
  }


}

