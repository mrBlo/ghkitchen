package com.bpm.user;

//import com.blo.kitchenGh.R;

import com.bpm.ghkitchen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MyRecipesPage extends Activity{
Button add,view;
	
String username,password;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myrecipes);
		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");
		
		
		add= (Button) findViewById(R.id.addrecipebtn);
		view= (Button) findViewById(R.id.viewrecipebtn);
		
		
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 Intent i = new Intent(getApplicationContext(),AddRecipe.class);
				  i.putExtra("username",username);
				  i.putExtra("password",password);
				  startActivity(i);
				  finish();			}
		});
		
		
		
		
		
		view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
//				startActivity(new Intent(getApplicationContext(), ViewYourRecipes.class));
//				MyRecipesPage.this.finish();
			}
		});
		
		
		
		
		
		
	}
	
	
	@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			//super.onBackPressed();
		 Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
		  i.putExtra("username",username);
		  i.putExtra("password",password);
		  startActivity(i);
		  finish();
		  }
	
}
