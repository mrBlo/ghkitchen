package com.bpm.user;

import com.bpm.ghkitchen.HomePage;
import com.bpm.ghkitchen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class UserPage1 extends Activity{
Button signin,signup;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.userpage1);
		signin= (Button) findViewById(R.id.SignInBtn);
		signup= (Button) findViewById(R.id.SignUpBtn);
		
		signin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), SignInPage.class));
				UserPage1.this.finish();
			}
		});
		
		
		signup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(getApplicationContext(), SignUpPage.class));
				UserPage1.this.finish();
			}
		});
		
	}
	
	@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			UserPage1.this.finish();
	startActivity(new Intent(getApplicationContext(), HomePage.class));		
	}
}
